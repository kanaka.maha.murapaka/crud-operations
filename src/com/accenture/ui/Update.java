package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.Product;

public class Update {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1"); // load database configuration,
		// open database
		EntityManager em = factory.createEntityManager(); // rest of the operations will be handled by entitymanager

		em.getTransaction().begin();

		//code for update record
		Product temp = em.find(Product.class, 104);
		
		if(temp != null) {
			temp.setpName("dell laptop");
			temp.setPrice(77000);
		}else {
			System.err.println("could not update product as it is not available");
		}		

		em.getTransaction().commit();

		em.close();
		factory.close();

	}
}
